//
//  AppDelegate.h
//  Interpay
//
//  Created by Umaid Naeem on 30/05/2015.
//  Copyright (c) 2015 umaid.SemiticSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong)UINavigationController *navigationController;

@property (nonatomic, strong)ViewController *viewController;

@end

