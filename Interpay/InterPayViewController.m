//
//  InterPayViewController.m
//  Interpay
//
//  Created by Umaid Naeem on 30/05/2015.
//  Copyright (c) 2015 umaid.SemiticSoft. All rights reserved.
//

#import "InterPayViewController.h"

@interface InterPayViewController ()
{
    UIView *headerView;
    UILabel *interpayLabel;
    UIButton *menuButton;
    
}
@end

@implementation InterPayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor colorWithRed:234.0/256.0 green:234.0/256.0 blue:234.0/256.0 alpha:1.0]];
    
    
    [self initializeMethod];
    [self setUpUserInterface];
    
}

-(void)initializeMethod
{
    headerView = [[UIView alloc] init];
    interpayLabel = [[UILabel alloc] init];
    menuButton = [UIButton buttonWithType:UIButtonTypeCustom];

}

-(void)setUpUserInterface
{
    CGRect headerViewFrame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, 44);
    [headerView setFrame:headerViewFrame];
    [headerView setBackgroundColor:[UIColor colorWithRed:74.0/256.0 green:101.0/256.0 blue:160.0/256.0 alpha:1.0]];
    [self.view addSubview:headerView];
    
    
    UIImage *menuImage = [UIImage imageNamed:@"menu.png"];
    CGRect menuFrame = CGRectMake(8, 8, menuImage.size.width, menuImage.size.height);
    [menuButton setFrame:menuFrame];
    [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
    [headerView addSubview:menuButton];

    
    int x = menuButton.bounds.size.width + 25;
    int width = 80;
    int height = 30;
    int y = (headerView.frame.size.height - height)/2;
    
    CGRect interPayLabelFrame = CGRectMake(x, y, width, height);
    [interpayLabel setFrame:interPayLabelFrame];
    [interpayLabel setText:@"Interpay"];
    [interpayLabel setTextColor:[UIColor whiteColor]];
    [interpayLabel setTextAlignment:NSTextAlignmentCenter];
    [headerView addSubview:interpayLabel];
    
    
    
    
    
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
