//
//  LoginViewController.m
//  Interpay
//
//  Created by Umaid Naeem on 30/05/2015.
//  Copyright (c) 2015 umaid.SemiticSoft. All rights reserved.
//

#import "LoginViewController.h"
#import "InterPayViewController.h"

@interface LoginViewController ()
{
    UILabel *interPayLabel;
    UITextField *emailTextFiled;
    UITextField *userNameTextFiled;
    UIButton *signInButton;
    
    UIKeyboardViewController *keyboardController;

}
@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self initializaMethod];
    [self setUpUSerInterface];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    keyboardController = [[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    
    [keyboardController addToolbarToKeyboard];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}


-(void)initializaMethod
{
    interPayLabel = [[UILabel alloc] init];
    
    emailTextFiled = [[UITextField alloc] init];
    
    userNameTextFiled = [[UITextField alloc] init];
    
    signInButton = [UIButton buttonWithType:UIButtonTypeCustom];

}

-(void)setUpUSerInterface
{
    int width = 150;
    int height = 50;
    int gapFromAbove = 100;
    int x = ([[UIScreen mainScreen]bounds].size.width - width)/2;
    int y = ([[UIScreen mainScreen]bounds].size.height - height)/2 - gapFromAbove;
    
    
    CGRect interpayLabelFrame = CGRectMake(x, y, width, height);
    [interPayLabel setFrame:interpayLabelFrame];
    [interPayLabel setText:@"interpay"];
    [interPayLabel setTextAlignment:NSTextAlignmentCenter];
    [interPayLabel setFont:[UIFont boldSystemFontOfSize:30.0]];
    [interPayLabel setTextColor:[UIColor blackColor]];
    [self.view addSubview:interPayLabel];
    
    
    int gap = 80;
    y += gap;
    width = [[UIScreen mainScreen]bounds].size.width - 20;
    x = ([[UIScreen mainScreen]bounds].size.width - width)/2;
    
    CGRect emailTextFiledFrame = CGRectMake(x, y, width, 25);
    [emailTextFiled setFrame:emailTextFiledFrame];
    [emailTextFiled setPlaceholder:@"Email / Mobile / Username"];
    [emailTextFiled setBorderStyle:UITextBorderStyleRoundedRect];
    [emailTextFiled setDelegate:self];
    [self.view addSubview:emailTextFiled];
    
    y += emailTextFiled.bounds.size.height + 10;
    
    CGRect userNameTextFiledFrame = CGRectMake(x, y, width, 25);
    [userNameTextFiled setFrame:userNameTextFiledFrame];
    [userNameTextFiled setPlaceholder:@"Password"];
    [userNameTextFiled setBorderStyle:UITextBorderStyleRoundedRect];
    [userNameTextFiled setDelegate:self];
    [self.view addSubview:userNameTextFiled];
    
    width = 60;
    y += userNameTextFiled.bounds.size.height + 10;
    x = [[UIScreen mainScreen]bounds].size.width/2 - width + 150;
    
    CGRect signInButtonFrame = CGRectMake(x, y, width, 40);
    [signInButton setFrame:signInButtonFrame];
    [signInButton setBackgroundColor:[UIColor colorWithRed:74.0/256.0 green:101.0/256.0 blue:161.0/256.0 alpha:1.0]];
    [signInButton setTitle:@"Sign in" forState:UIControlStateNormal];
    [signInButton.layer setBorderWidth:1.0];
    [signInButton.layer setBorderColor:[UIColor colorWithRed:74.0/256.0 green:101.0/256.0 blue:161.0/256.0 alpha:1.0].CGColor];
    [signInButton.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
    [signInButton addTarget:self action:@selector(InterPayMethod) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:signInButton];
    
    

 
}

-(void)InterPayMethod
{
    InterPayViewController *interPayObject = [[InterPayViewController alloc] init];
    [self.navigationController pushViewController:interPayObject animated:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
