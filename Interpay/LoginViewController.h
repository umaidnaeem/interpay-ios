//
//  LoginViewController.h
//  Interpay
//
//  Created by Umaid Naeem on 30/05/2015.
//  Copyright (c) 2015 umaid.SemiticSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"

@interface LoginViewController : UIViewController<UIKeyboardViewControllerDelegate,UITextFieldDelegate>

@end
