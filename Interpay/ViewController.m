//
//  ViewController.m
//  Interpay
//
//  Created by Umaid Naeem on 30/05/2015.
//  Copyright (c) 2015 umaid.SemiticSoft. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    UILabel *interPayLabel;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    int width = 150;
    int height = 50;
    int x = ([[UIScreen mainScreen]bounds].size.width - width)/2;
    int y = ([[UIScreen mainScreen]bounds].size.height - height)/2;
    
    
    CGRect interpayLabelFrame = CGRectMake(x, y, width, height);
    interPayLabel = [[UILabel alloc] init];
    [interPayLabel setFrame:interpayLabelFrame];
    [interPayLabel setText:@"interpay"];
    [interPayLabel setTextAlignment:NSTextAlignmentCenter];
    [interPayLabel setFont:[UIFont boldSystemFontOfSize:30.0]];
    [interPayLabel setTextColor:[UIColor blackColor]];
    [self.view addSubview:interPayLabel];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
